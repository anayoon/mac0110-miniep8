function compareByValue(x,y)
	aux=0
	auy=0
	if x[1]=='1'#se o x[1] é 1 quer dizer que é 10
		aux=Int(10)
	elseif x[1]=='J'
		aux=Int(11)
	elseif x[1]=='Q'
		aux=Int(12)
	elseif x[1]=='K'
		aux=Int(13)
	elseif x[1]=='A'
		aux=14
	else
		aux=Int(x[1])-48
	end
	if y[1]=='1'
		auy=Int(10)
	elseif y[1]=='J'
		auy=Int(11)
	elseif y[1]=='Q'
		auy=Int(12)
	elseif y[1]=='K'
		auy=Int(13)
	elseif y[1]=='A'
		auy=Int(14)
	else
		auy=Int(y[1])-48
	end
	if aux<auy
		return true
	else
		return false
	end
end

function troca(v,i,j)
	aux=v[i]
	v[i]=v[j]
	v[j]=aux
end

function insercao_compareByValue(v)
	tam = length(v)
	for i in 2:tam
	j = i
	while j > 1
		if compareByValue(v[j], v[j - 1])
			troca(v, j, j - 1)
		else
			break
		end
		j = j - 1
	end
	end
	return v
end

function compareByValueAndSuit(x,y)
#se o naipe deles for diferente, eu já sei qual é o menor
#se o naipe for igual eu comparo igual à função compare_by_value
	if length(x)==3#se a length é 3 o naipe está na terceira casa
		jx=3
	else
		jx=2
	end
	if length(y)==3
		jy=3
	else
		jy=2
	end
	if x[jx]!=y[jy]#se o naipe deles for diferente, eu só comparo o naipe
		if x[jx]=='♦'
			ax=1
		elseif x[jx]=='♠'
			ax=2
		elseif x[jx]=='♥'
			ax=3
		elseif x[jx]=='♣'
			ax=4
		end
		if y[jy]=='♦'
			ay=1
		elseif y[jy]=='♠'
			ay=2
		elseif y[jy]=='♥'
			ay=3
		elseif y[jy]=='♣'
			ay=4
		end
		if ax<ay
			return true
		else
			return false
		end
	else#x[jx]==x[jy]
		aux=0
		auy=0
		if x[1]=='1'#se o x[1] é 1 quer dizer que é 10
			aux=Int(10)
		elseif x[1]=='J'
			aux=Int(11)
		elseif x[1]=='Q'
			aux=Int(12)
		elseif x[1]=='K'
			aux=Int(13)
		elseif x[1]=='A'
			aux=14
		else
			aux=Int(x[1])-48
		end
		if y[1]=='1'
			auy=Int(10)
		elseif y[1]=='J'
			auy=Int(11)
		elseif y[1]=='Q'
			auy=Int(12)
		elseif y[1]=='K'
			auy=Int(13)
		elseif y[1]=='A'
			auy=Int(14)
		else
			auy=Int(y[1])-48
		end
		if aux<auy
			return true
		else
			return false
		end
	end
end

function insercao_compareByValueAndSuit(v)
	tam = length(v)
	for i in 2:tam
	j = i
	while j > 1
		if compareByValueAndSuit(v[j], v[j - 1])
			troca(v, j, j - 1)
		else
			break
		end
		j = j - 1
	end
	end
	return v
end

using Test

function test_insercao_compareByValue()
	@test  insercao_compareByValue(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"])== ["10♥","10♦","J♠","K♠","A♠","A♠"]
	@test  insercao_compareByValue(["9♥", "5♦", "K♥", "A♥", "J♦","2♣"])== ["2♣", "5♦", "9♥", "J♦", "K♥", "A♥"]
	@test  insercao_compareByValue(["9♣", "9♦", "9♥", "9♠"])== ["9♣", "9♦", "9♥", "9♠"]
	println("Final dos testes da função insercao_compareByValue")
end

function test_insercao_compareByValueAndSuit()
	@test  insercao_compareByValueAndSuit(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"])== ["10♦","J♠","K♠","A♠","A♠","10♥"]
	@test  insercao_compareByValueAndSuit(["9♥", "5♦", "K♥", "A♥", "J♦", "2♠","2♣"])== ["5♦", "J♦", "2♠", "9♥", "K♥", "A♥", "2♣"]
	@test  insercao_compareByValueAndSuit(["9♣", "9♦", "9♥", "9♠"])== ["9♦", "9♠", "9♥", "9♣"]
	println("Final dos testes da função insercao_compareByValueAndSuit")
end

function test()
	test_insercao_compareByValue()
	test_insercao_compareByValueAndSuit()
end

test()
